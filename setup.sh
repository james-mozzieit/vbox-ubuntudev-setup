#!/usr/bin/env bash

# Add Needed Repositories
sudo add-apt-repository ppa:klaus-vormweg/ppa

# Upgrade Base Packages
sudo apt-get update
sudo apt-get upgrade -y

# Install Web Packages
sudo apt-get install -y build-essential dkms apache2 php5 php5-dev php5-json php5-mysql curl php5-curl libmcrypt4 php5-mcrypt openssh-server git terminator

# Setup Git
git config --global user.email "tjlingham@gmail.com"
git config --global user.name "Tom Lingham"
git config --global push.default simple

# Download Bash Aliases
wget -O ~/.bash_aliases https://bitbucket.org/tjlingham/vbox-ubuntudev-setup/raw/master/aliases

# Set Apache ServerName
sudo sed -i "s/#ServerRoot.*/ServerName ubuntu/" /etc/apache2/apache2.conf
sudo sed -i "s#DocumentRoot /var/www#DocumentRoot /media/sf_web#" /etc/apache2/apache2.conf
sudo /etc/init.d/apache2 restart

# Configure Mcrypt
sudo php5enmod mcrypt
sudo service apache2 restart

# Install Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Enable PHP Error Reporting
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/cli/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/cli/php.ini
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php5/cli/php.ini
sudo sed -i "s/;date.timezone.*/date.timezone = UTC/" /etc/php5/cli/php.ini

# Install NodeJs
echo 'export PATH=$HOME/local/bin:$PATH' >> ~/.bashrc
. ~/.bashrc
mkdir ~/local
mkdir ~/node-latest-install
cd ~/node-latest-install
curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
./configure --prefix=~/local
make install
curl https://www.npmjs.org/install.sh | sh
rm ~/node-latest-install
sudo chown -R tom local

# Install Awesome WM
sudo apt-get install -y awesome alsa-utils imagemagick lua5.2 lua-lgi xinit terminator

# Add Awesome to startx
echo "exec awesome" > /home/tom/.xinitrc

# Download Terminator Config
sudo mkdir ~/.config/terminator
sudo wget -O ~/.config/terminator/config https://bitbucket.org/tjlingham/vbox-ubuntudev-setup/raw/master/terminator/config

# Install Awesome Themes
git clone https://github.com/copycat-killer/awesome-copycats.git ~/.config/awesome
git clone https://github.com/copycat-killer/lain.git ~/.config/awesome/lain
wget -O ~/.config/awesome/rc.lua https://bitbucket.org/tjlingham/vbox-ubuntudev-setup/raw/master/rc.lua
sudo sed -i "s/;date.timezone.*/date.timezone = UTC/" /etc/php5/cli/php.ini

# Install Grunt
npm install -g grunt-cli

# Install Bower
npm install -g bower
sudo chown -R tom .config

# VirtualBox Guest Additions
sudo mount /dev/cdrom /media/cdrom
sudo sh /media/cdrom/VBoxLinuxAdditions.run
sudo usermod -aG vboxsf www-data
sudo usermod -aG vboxsf tom

# Final Clean
cd ~
rm -rf tmp/

# Reboot
sudo reboot