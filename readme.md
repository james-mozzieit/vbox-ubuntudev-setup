Requires Ubuntu Server 14.04 LTS clean installation

Be sure to mount the VirtualBox Guest Additions ISO in VirtualBox (Devices > Insert Guest Additions CD Image...)

Type the following in terminal:

    wget -O setup.sh http://goo.gl/FgPoSm
    sudo bash setup.sh

Wait for the system to reboot, login and type `startx`. This will launch Awesome WM and start up a terminal.

You may want to go through the script and change any personal references to your own details after downloading the script using `wget`. I plan on creating this with a `whoami` variable in the future, but haven't got that far at this stage.

This scripts installs:

+ PHP
+ Apache
+ Git
+ Custome Aliases
+ Composer
+ NodeJs and NPM
+ Bower
+ Grunt CLI
+ Awesome Window Manager (Along with some nice themes)
+ Terminator terminal
+ VBox Guest Additions